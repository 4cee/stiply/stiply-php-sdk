<?php

namespace Tests;

use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use Tests\Stubs\DataObject;

/**
 * @covers \Stiply\Support\DataObject
 * @internal
 */
final class DataObjectTest extends TestCase
{
    /**
     * It creates a new data object instance.
     *
     * @return void
     */
    public function testCreateDataObject() : void
    {
        $object = new DataObject(['message' => 'Hey now!']);

        self::assertInstanceOf(DataObject::class, $object);
    }

    /**
     * It creates a new data object instance from a Guzzle response.
     *
     * @return void
     */
    public function testCreateDataObjectFromResponse() : void
    {
        $response = new Response(200, [], json_encode(['message' => 'Hey now!']));

        $object = DataObject::fromResponse($response);

        self::assertInstanceOf(DataObject::class, $object);
        self::assertSame('Hey now!', $object->message);
    }

    /**
     * It gets an attribute.
     *
     * @return void
     */
    public function testGetAttribute() : void
    {
        $object = new DataObject(['message' => 'Hey now!']);

        self::assertSame('Hey now!', $object->message);
        self::assertSame('Hey now!', $object->getAttribute('message'));
        self::assertSame('Hey now!', $object['message']);
    }

    /**
     * It sets an attribute.
     *
     * @return void
     */
    public function testSetAttribute() : void
    {
        $object = new DataObject();

        $object->message = 'Hey now!';
        self::assertSame('Hey now!', $object->message);

        $object->setAttribute('message', 'foo');
        self::assertSame('foo', $object->message);

        $object['message'] = 'bar';
        self::assertSame('bar', $object->message);
    }

    /**
     * It gets a nested attribute.
     *
     * @return void
     */
    public function testGetNestedAttribute() : void
    {
        $object = new DataObject(['message' => ['foo' => 'bar']]);

        self::assertSame('bar', $object->message['foo']);
        self::assertSame('bar', $object->getAttribute('message.foo'));
        self::assertSame('bar', $object['message.foo']);
    }

    /**
     * It sets a nested attribute.
     *
     * @return void
     */
    public function testSetNestedAttribute() : void
    {
        $object = new DataObject();

        $object->setAttribute('message.foo', 'bar');
        self::assertSame('bar', $object['message.foo']);

        $object['message.foo'] = 'bar';
        self::assertSame('bar', $object['message.foo']);
    }

    /**
     * It determines if the attribute is set.
     *
     * @return void
     */
    public function testHasAttribute() : void
    {
        $object = new DataObject();

        self::assertFalse($object->hasAttribute('message'));
        self::assertFalse(isset($object->message));
        self::assertEmpty($object->message);
    }

    /**
     * It removes an attribute.
     *
     * @return void
     */
    public function testRemoveAttribute() : void
    {
        $object = new DataObject(['message' => 'Hey now!']);

        unset($object['message']);

        self::assertEmpty($object->message);
    }

    /**
     * It removes a nested attribute.
     *
     * @return void
     */
    public function testRemoveNestedAttribute() : void
    {
        $object = new DataObject(['message' => ['foo' => 'bar']]);

        unset($object['message.foo']);

        self::assertEmpty($object['message.foo']);
        self::assertEmpty($object->getAttribute('message.foo'));
    }

    /**
     * It gets an attribute through an accessor.
     *
     * @return void
     */
    public function testGetAttributeThroughAccessor() : void
    {
        $object = new DataObject(['slug_message' => 'Hey now!']);

        self::assertSame('hey-now', $object->slug_message);
        self::assertSame('hey-now', $object->getAttribute('slug_message'));
        self::assertSame('hey-now', $object['slug_message']);
    }

    /**
     * It sets an attribute through a mutator.
     *
     * @return void
     */
    public function testSetAttributeThroughMutator() : void
    {
        $object = new DataObject();

        $object->title_message = 'hey now!';
        self::assertSame('Hey Now!', $object->title_message);

        $object->setAttribute('title_message', 'hey now!');
        self::assertSame('Hey Now!', $object->title_message);

        $object['title_message'] = 'hey now!';
        self::assertSame('Hey Now!', $object->title_message);
    }

    /**
     * It casts a data object to an array.
     *
     * @return void
     */
    public function testToArray() : void
    {
        $object = new DataObject(['message' => 'Hey now!']);

        self::assertSame(['message' => 'Hey now!'], $object->toArray());
    }
}
