<?php

namespace Tests;

use Faker\Factory;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Illuminate\Support\Str;
use PHPUnit\Framework\TestCase;
use Stiply\Api;
use Stiply\CreateSigner;
use Stiply\CreateSignRequest;
use Stiply\DeleteSigner;
use Stiply\DeleteSignRequest;
use Stiply\Exceptions\StiplyException;
use Stiply\ExtendTerm;
use Stiply\GetProofDocument;
use Stiply\GetSignedDocument;
use Stiply\GetSigner;
use Stiply\GetSignRequest;
use Stiply\GetSignRequestKey;
use Stiply\Ping;
use Stiply\SendReminder;
use Stiply\SendSignRequest;
use Stiply\UpdateSigner;
use Tests\Stubs\ResponseData;
use Tests\Stubs\TokenRepository;

/**
 * @covers \Stiply\Api
 * @internal
 */
final class ApiTest extends TestCase
{
    /**
     * The token repository implementation.
     *
     * @var \Stiply\Contracts\Auth\TokenRepository
     */
    private $tokens;

    /**
     * The Faker implementation.
     *
     * @var \Faker\Generator
     */
    private $faker;

    /**
     * Set up for all tests.
     *
     * @return void
     */
    protected function setUp() : void
    {
        $this->tokens = new TokenRepository();

        $this->faker = Factory::create();
    }

    /**
     * It creates a new API instance.
     *
     * @return void
     */
    public function testCreateApi() : void
    {
        self::assertInstanceOf(Api::class, Api::create($this->tokens));
    }

    /**
     * It pings the API.
     *
     * @return void
     */
    public function testPing() : void
    {
        $handler = new MockHandler([
            new Response(200, [], json_encode(ResponseData::PING)),
        ]);

        $api = Api::mock($this->tokens, $handler);

        $result = $api->ping(['ping_value' => 'pong']);

        self::assertInstanceOf(Ping::class, $result);
        self::assertSame(ResponseData::PING, $result->toArray());
    }

    /**
     * It creates a sign request.
     *
     * @return void
     */
    public function testCreateSignRequest() : void
    {
        $handler = new MockHandler([
            new Response(200, [], json_encode(ResponseData::CREATE_SIGN_REQUEST)),
        ]);

        $api = Api::mock($this->tokens, $handler);

        $result = $api->createSignRequest([
            'file' => file_get_contents(__DIR__.\DIRECTORY_SEPARATOR.'Stubs'.\DIRECTORY_SEPARATOR.'dummy.pdf'),
            'filename' => 'dummy.pdf',
            'term' => '1w',
        ]);

        self::assertInstanceOf(CreateSignRequest::class, $result);
        self::assertSame(ResponseData::CREATE_SIGN_REQUEST, $result->toArray());
    }

    /**
     * It creates a signer.
     *
     * @return void
     */
    public function testCreateSigner() : void
    {
        $handler = new MockHandler([
            new Response(200, [], json_encode(ResponseData::CREATE_SIGNER)),
        ]);

        $api = Api::mock($this->tokens, $handler);

        $result = $api->createSigner(Str::random(40), [
            'signer_email' => $this->faker->safeEmail,
            'signer_signature_fields' => [['name' => 'signature_1']],
        ]);

        $api = Api::mock($this->tokens, $handler);

        self::assertInstanceOf(CreateSigner::class, $result);
        self::assertSame(ResponseData::CREATE_SIGNER, $result->toArray());
    }

    /**
     * It sends a sign request.
     *
     * @return void
     */
    public function testSendSignRequest() : void
    {
        $handler = new MockHandler([
            new Response(200, [], json_encode(ResponseData::SEND_SIGN_REQUEST)),
        ]);

        $api = Api::mock($this->tokens, $handler);

        $result = $api->sendSignRequest(Str::random(40));

        $api = Api::mock($this->tokens, $handler);

        self::assertInstanceOf(SendSignRequest::class, $result);
        self::assertSame(ResponseData::SEND_SIGN_REQUEST, $result->toArray());
    }

    /**
     * It gets a signed document.
     *
     * @return void
     */
    public function testGetSignedDocument() : void
    {
        $handler = new MockHandler([
            new Response(200, [], json_encode(ResponseData::GET_SIGNED_DOCUMENT)),
        ]);

        $api = Api::mock($this->tokens, $handler);

        $result = $api->getSignedDocument(Str::random(40));

        $api = Api::mock($this->tokens, $handler);

        self::assertInstanceOf(GetSignedDocument::class, $result);
        self::assertSame(ResponseData::GET_SIGNED_DOCUMENT, $result->toArray());
    }

    /**
     * It gets a proof document.
     *
     * @return void
     */
    public function testGetProofDocument() : void
    {
        $handler = new MockHandler([
            new Response(200, [], json_encode(ResponseData::GET_PROOF_DOCUMENT)),
        ]);

        $api = Api::mock($this->tokens, $handler);

        $result = $api->getProofDocument(Str::random(40));

        $api = Api::mock($this->tokens, $handler);

        self::assertInstanceOf(GetProofDocument::class, $result);
        self::assertSame(ResponseData::GET_PROOF_DOCUMENT, $result->toArray());
    }

    /**
     * It sends a reminder mail to the current signer.
     *
     * @return void
     */
    public function testSendReminder() : void
    {
        $handler = new MockHandler([
            new Response(200, [], json_encode(ResponseData::SEND_REMINDER)),
        ]);

        $api = Api::mock($this->tokens, $handler);

        $result = $api->sendReminder(Str::random(40));

        $api = Api::mock($this->tokens, $handler);

        self::assertInstanceOf(SendReminder::class, $result);
        self::assertSame(ResponseData::SEND_REMINDER, $result->toArray());
    }

    /**
     * It extends the term of a sign request.
     *
     * @return void
     */
    public function testExtendTerm() : void
    {
        $handler = new MockHandler([
            new Response(200, [], json_encode(ResponseData::EXTEND_TERM)),
        ]);

        $api = Api::mock($this->tokens, $handler);

        $result = $api->extendTerm(Str::random(40), ['term' => '2d']);

        $api = Api::mock($this->tokens, $handler);

        self::assertInstanceOf(ExtendTerm::class, $result);
        self::assertSame(ResponseData::EXTEND_TERM, $result->toArray());
    }

    /**
     * It gets the sign request key from an external key.
     *
     * @return void
     */
    public function testGetSignRequestKey() : void
    {
        $handler = new MockHandler([
            new Response(200, [], json_encode(ResponseData::GET_SIGN_REQUEST_KEY)),
        ]);

        $api = Api::mock($this->tokens, $handler);

        $result = $api->getSignRequestKey(Str::random(40));

        $api = Api::mock($this->tokens, $handler);

        self::assertInstanceOf(GetSignRequestKey::class, $result);
        self::assertSame(ResponseData::GET_SIGN_REQUEST_KEY, $result->toArray());
    }

    /**
     * It gets a sign request.
     *
     * @return void
     */
    public function testGetSignRequest() : void
    {
        $handler = new MockHandler([
            new Response(200, [], json_encode(ResponseData::GET_SIGN_REQUEST)),
        ]);

        $api = Api::mock($this->tokens, $handler);

        $result = $api->getSignRequest(Str::random(40));

        $api = Api::mock($this->tokens, $handler);

        self::assertInstanceOf(GetSignRequest::class, $result);
        self::assertSame(ResponseData::GET_SIGN_REQUEST, $result->toArray());
    }

    /**
     * It deletes a sign request.
     *
     * @return void
     */
    public function testDeleteSignRequest() : void
    {
        $handler = new MockHandler([
            new Response(200, [], json_encode(ResponseData::DELETE_SIGN_REQUEST)),
        ]);

        $api = Api::mock($this->tokens, $handler);

        $result = $api->deleteSignRequest(Str::random(40));

        $api = Api::mock($this->tokens, $handler);

        self::assertInstanceOf(DeleteSignRequest::class, $result);
        self::assertSame(ResponseData::DELETE_SIGN_REQUEST, $result->toArray());
    }

    /**
     * It gets a signer.
     *
     * @return void
     */
    public function testGetSigner() : void
    {
        $handler = new MockHandler([
            new Response(200, [], json_encode(ResponseData::GET_SIGNER)),
        ]);

        $api = Api::mock($this->tokens, $handler);

        $result = $api->getSigner(Str::random(40), Str::random(40));

        $api = Api::mock($this->tokens, $handler);

        self::assertInstanceOf(GetSigner::class, $result);
        self::assertSame(ResponseData::GET_SIGNER, $result->toArray());
    }

    /**
     * It updates a signer.
     *
     * @return void
     */
    public function testUpdateSigner() : void
    {
        $handler = new MockHandler([
            new Response(200, [], json_encode(ResponseData::UPDATE_SIGNER)),
        ]);

        $api = Api::mock($this->tokens, $handler);

        $result = $api->updateSigner(Str::random(40), Str::random(40), [
            'new_email' => $this->faker->safeEmail,
            'new_cell_phone_number' => '+31612345678',
        ]);

        $api = Api::mock($this->tokens, $handler);

        self::assertInstanceOf(UpdateSigner::class, $result);
        self::assertSame(ResponseData::UPDATE_SIGNER, $result->toArray());
    }

    /**
     * It deletes a signer.
     *
     * @return void
     */
    public function testDeleteSigner() : void
    {
        $handler = new MockHandler([
            new Response(200, [], json_encode(ResponseData::DELETE_SIGNER)),
        ]);

        $api = Api::mock($this->tokens, $handler);

        $result = $api->deleteSigner(Str::random(40), Str::random(40));

        $api = Api::mock($this->tokens, $handler);

        self::assertInstanceOf(DeleteSigner::class, $result);
        self::assertSame(ResponseData::DELETE_SIGNER, $result->toArray());
    }

    /**
     * It throws an exception when there is an error.
     *
     * @return void
     */
    public function testThrowOnError() : void
    {
        $handler = new MockHandler([
            new RequestException('Hey now!', new Request('POST', '/actions/ping')),
        ]);

        $api = Api::mock($this->tokens, $handler);

        self::expectException(StiplyException::class);
        self::expectExceptionCode(0);
        self::expectExceptionMessage('Hey now!');

        $api->ping(['ping_value' => 'pong']);
    }
}
