<?php

namespace Tests;

use PHPUnit\Framework\TestCase;
use Stiply\Config\Config;

/**
 * @covers \Stiply\Config\Config
 * @internal
 */
final class ConfigTest extends TestCase
{
    /**
     * It returns a singleton instance.
     *
     * @return void
     */
    public function testConfigSingleton() : void
    {
        $config = Config::getInstance();

        self::assertSame($config, Config::setInstance($config));

        Config::setInstance(null);

        $config2 = Config::getInstance();

        self::assertInstanceOf(Config::class, $config2);
        self::assertNotSame($config, $config2);
    }

    /**
     * It retrieves all default config variables.
     *
     * @return void
     */
    public function testRetrieveDefaultConfig() : void
    {
        $config = Config::getInstance();

        self::assertSame('https://api.stiply.nl/', $config['base_url']);
        self::assertSame('v1.1', $config['version']);
    }

    /**
     * It updates the default config.
     *
     * @return void
     */
    public function testRetrieveUpdatedConfig() : void
    {
        $config = Config::getInstance();

        $config->set('base_url', 'https://api.local.stiply.nl/');

        self::assertSame('https://api.local.stiply.nl/', $config['base_url']);
        self::assertSame('v1.1', $config['version']);
    }

    /**
     * It updates a specific config item.
     *
     * @return void
     */
    public function testUpdateConfigItem() : void
    {
        $config = Config::getInstance();

        self::assertSame('v1.1', $config['version']);

        $config['version'] = 'v2';

        self::assertSame('v2', $config['version']);
    }
}
