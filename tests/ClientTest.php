<?php

namespace Tests;

use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use Stiply\Clients\Client;
use Stiply\Config\Config;
use Stiply\Exceptions\AuthException;
use Stiply\Exceptions\StiplyException;
use Tests\Stubs\TokenRepository;

/**
 * @covers \Stiply\Clients\Client
 * @internal
 */
final class ClientTest extends TestCase
{
    /**
     * The token repository implementation.
     *
     * @var \Stiply\Contracts\Auth\TokenRepository
     */
    private $tokens;

    /**
     * The Guzzle mock handler implementation.
     *
     * @var \GuzzleHttp\Handler\MockHandler
     */
    private $handler;

    /**
     * Set up for all tests.
     *
     * @return void
     */
    protected function setUp() : void
    {
        $this->tokens = new TokenRepository();

        $this->handler = new MockHandler([
            new Response(200, [], json_encode('Hey now!')),
        ]);
    }

    /**
     * Tear down for all tests.
     *
     * @return void
     */
    protected function tearDown() : void
    {
        $this->handler->reset();
    }

    /**
     * It creates a new client instance.
     *
     * @return void
     */
    public function testCreateClient() : void
    {
        self::assertInstanceOf(Client::class, Client::create($this->tokens));
    }

    /**
     * It performs a GET request.
     *
     * @return void
     */
    public function testPerformGetRequest() : void
    {
        $client = Client::mock($this->tokens, $this->handler);

        $response = $client->get('/');

        self::assertSame(200, $response->getStatusCode());
        self::assertSame('Hey now!', json_decode($response->getBody(), true));
    }

    /**
     * It performs a POST request.
     *
     * @return void
     */
    public function testPerformPostRequest() : void
    {
        $client = Client::mock($this->tokens, $this->handler);

        $response = $client->post('/');

        self::assertSame(200, $response->getStatusCode());
        self::assertSame('Hey now!', json_decode($response->getBody(), true));
    }

    /**
     * It performs a PUT request.
     *
     * @return void
     */
    public function testPerformPutRequest() : void
    {
        $client = Client::mock($this->tokens, $this->handler);

        $response = $client->put('/');

        self::assertSame(200, $response->getStatusCode());
        self::assertSame('Hey now!', json_decode($response->getBody(), true));
    }

    /**
     * It performs a DELETE request.
     *
     * @return void
     */
    public function testPerformDeleteRequest() : void
    {
        $client = Client::mock($this->tokens, $this->handler);

        $response = $client->delete('/');

        self::assertSame(200, $response->getStatusCode());
        self::assertSame('Hey now!', json_decode($response->getBody(), true));
    }

    /**
     * It throws an exception when there is an error.
     *
     * @return void
     */
    public function testThrowOnError() : void
    {
        self::expectException(StiplyException::class);
        self::expectExceptionCode(0);
        self::expectExceptionMessage('Hey now!');

        $handler = new MockHandler([
            new RequestException('Hey now!', new Request('GET', '/')),
        ]);

        $client = Client::mock($this->tokens, $handler);

        $client->get('/');
    }

    /**
     * It throws an exception when the configured API version and auth method
     * are not compatible.
     *
     * @return void
     */
    public function testThrowOnApiAndAuthMismatch() : void
    {
        self::expectException(AuthException::class);
        self::expectExceptionMessage('Auth method [oauth] for API [v1] is invalid.');

        $config = Config::getInstance();

        $config->set('version', 'v1');

        $client = Client::mock($this->tokens, $this->handler);

        $client->get('/');

        $this->handler->reset();

        self::expectException(AuthException::class);
        self::expectExceptionMessage('Auth method [basic] for API [v1.1] is invalid.');

        $config->set('version', 'v1.1');
        $config->set('auth', 'basic');

        $client->get('/');

        $this->handler->reset();

        self::expectException(AuthException::class);
        self::expectExceptionMessage('Auth method [jwt] for API [v1.1] is invalid.');

        $config->set('auth', 'jwt');

        $client->get('/');
    }
}
