<?php

namespace Tests\Stubs;

final class ResponseData
{
    /**
     * The "ping" response data.
     *
     * @var array
     */
    public const PING = ['ping_value' => 'pong', 'status_code' => '200'];

    /**
     * The "createSignRequest" response data.
     *
     * @var array
     */
    public const CREATE_SIGN_REQUEST = [
        'data' => [
            'document' => [
                'name' => 'End-user license agreement',
                'filename' => 'license-agreement.pdf',
                'file_size' => '224615',
                'created_at' => '2017-03-29T13:43:07+0200',
                'updated_at' => '2017-03-29T13:43:07+0200',
            ],
            'sign_request' => [
                'key' => 'jgxfx3qYLaL9A2th3XejaOoODEp2QahRr1s5b3ki062JMu1k9n',
                'sent' => 0,
                'sent_at' => '0000-00-00 00:00:00',
                'all_signed' => 0,
                'all_signed_at' => '0000-00-00 00:00:00',
                'signed_hash' => '',
                'proof_hash' => '',
                'message' => 'Hi! Could you please sign our license agreement? Cheers!',
                'comment' => 'Customer wants to start asap',
                'external_key' => 'myexternalkey1234',
                'call_back_url' => 'https://www.myawesomewebsitecallbackurl.com/callback',
                'create_at' => '2017-03-29T13:43:07+0200',
                'update_at' => '2017-03-29T13:43:07+0200',
                'terms' => [
                    [
                        'term_code' => '1w',
                        'term_expired' => 0,
                        'term_expires' => '0000-00-00 00:00:00',
                        'create_at' => '2017-03-29T13:43:07+0200',
                    ],
                ],
            ],
            'signers' => [],
        ],
    ];

    /**
     * The "createSigner" response data.
     *
     * @var array
     */
    public const CREATE_SIGNER = [
        'data' => [
            'key' => 'X7FylzU1MQlIaMEo4oec03lkQtI0wF2jtrEHQoXtotGDaDH5CI',
            'email' => 'signer@stiply.nl',
            'name' => null,
            'order' => 0,
            'language' => 'nl',
            'phone' => 31612345678,
            'auth_method' => 'sms',
            'role' => '',
            'sign_url' => null,
            'emandate' => [],
            'created_at' => '2017-03-29T14:59:56+0200',
            'updated_at' => '2017-03-29T14:59:56+0200',
            'signer_progresses' => [],
            'fields' => [
                [
                    'type' => 'signature',
                    'x' => 91.34,
                    'y' => 865.35,
                    'width' => 250,
                    'height' => 125,
                    'page' => 1,
                    'created_at' => '2017-03-29T14:59:57+0200',
                    'updated_at' => '2017-03-29T14:59:57+0200',
                ],
                [
                    'type' => 'signature',
                    'x' => 144.64,
                    'y' => 755.36,
                    'width' => 230,
                    'height' => 20,
                    'page' => 1,
                    'created_at' => '2017-03-29T14:59:57+0200',
                    'updated_at' => '2017-03-29T14:59:57+0200',
                ],
            ],
        ],
    ];

    /**
     * The "sendSignRequest" response data.
     *
     * @var array
     */
    public const SEND_SIGN_REQUEST = [
        'data' => [
            'document' => [
                'name' => 'End-user license agreement',
                'filename' => 'license-agreement.pdf',
                'file_size' => '224615',
                'created_at' => '2017-03-29T13:43:07+0200',
                'updated_at' => '2017-03-29T13:43:07+0200',
            ],
            'sign_request' => [
                'key' => 'jfxfx3qYLoL9AHth3XejOOoODEp2QahRr1s5b3bi062JMu1k9n',
                'sent' => 1,
                'sent_at' => '2017-03-29T15:12:17+0200',
                'all_signed' => 0,
                'all_signed_at' => '0000-00-00 00:00:00',
                'signed_hash' => '',
                'proof_hash' => '',
                'message' => 'Hi! Could you please sign our license agreement? Cheers!',
                'comment' => 'Customer wants to start asap',
                'external_key' => 'myexternalkey1234',
                'call_back_url' => 'https://www.myawesomewebsitecallbackurl.com/callback',
                'created_at' => '2017-03-29T13:43:07+0200',
                'updated_at' => '2017-03-29T15:12:17+0200',
                'terms' => [
                    [
                        'term_code' => '1w',
                        'term_expired' => 0,
                        'term_expires' => '2017-04-05T15:12:17+0200',
                        'created_at' => '2017-03-29T13:43:07+0200',
                    ],
                ],
                'signers' => [
                    [
                        'key' => 'X7FylzU1MQlIaMEo4oec03lkQtI0wF2jtrEHQoXtotGDaDH5CI',
                        'email' => 'signer@stiply.nl',
                        'name' => null,
                        'order' => null,
                        'phone' => 31612345678,
                        'auth_method' => 'sms',
                        'created_at' => '2017-03-29T14:59:56+0200',
                        'updated_at' => '2017-03-29T14:59:57+0200',
                        'sign_url' => null,
                        'signer_progresses' => [
                            [
                                'action' => 'mail_signrequest_sent',
                                'ip' => null,
                                'location' => null,
                                'system' => null,
                                'value' => 'signer@stiply.nl',
                                'created_at' => '2017-03-29T15:12:17+0200',
                                'status' => null,
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ];

    /**
     * The "getSignedDocument" response data.
     *
     * @var array
     */
    public const GET_SIGNED_DOCUMENT = [
        'file_name' => 'license-agreement-[Ondertekend].pdf',
        'mimeType' => 'application/pdf',
        'file_size' => '244087',
        'hash' => 'd24fe514aafcg15a79a9k3blb95b0237d0hf4d',
        'data' => 'bXktcGRm',
        'status_code' => '200',
    ];

    /**
     * The "getProofDocument" response data.
     *
     * @var array
     */
    public const GET_PROOF_DOCUMENT = [
        'file_name' => 'license-agreement-[Bewijs].pdf',
        'mimeType' => 'application/pdf',
        'file_size' => '244087',
        'hash' => '2289ac9gkei61bd5b2306f90e27743fkaa184e64a',
        'data' => 'bXktcGRm',
        'status_code' => '200',
    ];

    /**
     * The "sendReminder" response data.
     *
     * @var array
     */
    public const SEND_REMINDER = [
        'data' => [
            'document' => [
                'name' => 'End-user license agreement',
                'filename' => 'license-agreement.pdf',
                'file_size' => '224615',
                'created_at' => '2017-03-29T16:57:19+0200',
                'updated_at' => '2017-03-29T16:57:19+0200',
            ],
            'sign_request' => [
                'key' => 'eIGy1HCynD6B7QDo2gbsXeDoPKdjsLr1U8SNKGx1CMOzB77Peh',
                'sent' => 1,
                'sent_at' => '2017-03-29T16:58:16+0200',
                'all_signed' => 0,
                'all_signed_at' => '0000-00-00 00:00:00',
                'signed_hash' => '',
                'proof_hash' => '',
                'message' => 'Hi! Could you please sign our license agreement? Cheers!',
                'comment' => 'Customer wants to start asap',
                'external_key' => 'myexternalkey12345',
                'call_back_url' => 'https://www.myawesomewebsitecallbackurl.com/callback',
                'created_at' => '2017-03-29T16:57:19+0200',
                'updated_at' => '2017-03-29T16:58:16+0200',
                'terms' => [
                    [
                        'term_code' => '1w',
                        'term_expired' => 0,
                        'term_expires' => '2017-04-05T16:58:16+0200',
                        'created_at' => '2017-03-29T16:57:19+0200',
                    ],
                ],
                'signers' => [
                    [
                        'key' => 'X7FylzU1MQlIaMEo4oec03lkQtI0wF2jtrEHQoXtotGDaDH5CI',
                        'email' => 'signer@stiply.nl',
                        'name' => null,
                        'order' => null,
                        'phone' => 31612345678,
                        'auth_method' => 'sms',
                        'created_at' => '2017-03-29T16:58:05+0200',
                        'updated_at' => '2017-03-29T16:58:06+0200',
                        'sign_url' => null,
                        'signer_progresses' => [
                            [
                                'action' => 'mail_signrequest_sent',
                                'ip' => null,
                                'location' => null,
                                'system' => null,
                                'value' => 'signer@stiply.nl',
                                'created_at' => '2017-03-29T16:58:16+0200',
                                'status' => 'failed',
                            ],
                            [
                                'action' => 'mail_reminder_sent',
                                'ip' => null,
                                'location' => null,
                                'system' => null,
                                'value' => 'signer@stiply.nl',
                                'created_at' => '2017-03-29T16:58:27+0200',
                                'status' => null,
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ];

    /**
     * The "extendTerm" response data.
     *
     * @var array
     */
    public const EXTEND_TERM = [
        'data' => [
            'document' => [
                'name' => 'End-user license agreement',
                'filename' => 'license-agreement.pdf',
                'file_size' => '224615',
                'created_at' => '2017-03-29T16:57:19+0200',
                'updated_at' => '2017-03-29T16:57:19+0200',
            ],
            'sign_request' => [
                'key' => 'eIGy1HCynD6B7QDo2gbsXeDoPKdjsLr1U8SNKGx1CMOzB77Peh',
                'sent' => 1,
                'sent_at' => '2017-03-29T16:58:16+0200',
                'all_signed' => 0,
                'all_signed_at' => '0000-00-00 00:00:00',
                'signed_hash' => '',
                'proof_hash' => '',
                'message' => 'Hi! Could you please sign our license agreement? Cheers!',
                'comment' => 'Customer wants to start asap',
                'external_key' => 'myexternalkey12345',
                'call_back_url' => 'https://www.myawesomewebsitecallbackurl.com/callback',
                'created_at' => '2017-03-29T16:57:19+0200',
                'updated_at' => '2017-03-29T16:58:16+0200',
                'terms' => [
                    [
                        'term_code' => '1w',
                        'term_expired' => 1,
                        'term_expires' => '2017-04-05T16:58:16+0200',
                        'created_at' => '2017-03-29T16:57:19+0200',
                    ],
                    [
                        'term_code' => '2d',
                        'term_expired' => 0,
                        'term_expires' => '2017-04-08T09:00:00+0200',
                        'created_at' => '2017-04-06T09:00:00+0200',
                    ],
                ],
                'signers' => [
                    [
                        'key' => 'X7FylzU1MQlIaMEo4oec03lkQtI0wF2jtrEHQoXtotGDaDH5CI',
                        'email' => 'signer@stiply.nl',
                        'name' => null,
                        'order' => null,
                        'phone' => 31612345678,
                        'auth_method' => 'sms',
                        'created_at' => '2017-03-29T16:58:05+0200',
                        'updated_at' => '2017-03-29T16:58:06+0200',
                        'sign_url' => null,
                        'signer_progresses' => [
                            [
                                'action' => 'mail_signrequest_sent',
                                'ip' => null,
                                'location' => null,
                                'system' => null,
                                'value' => 'signer@stiply.nl',
                                'created_at' => '2017-03-29T16:58:16+0200',
                                'status' => 'failed',
                            ],
                            [
                                'action' => 'mail_term_extended_sent',
                                'ip' => null,
                                'location' => null,
                                'system' => null,
                                'value' => 'signer@stiply.nl',
                                'created_at' => '2017-03-29T16:58:27+0200',
                                'status' => null,
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ];

    /**
     * The "getSignRequestKey" response data.
     *
     * @var array
     */
    public const GET_SIGN_REQUEST_KEY = [
        'key' => 'eIGy1HCynD6B7QDo2gbsXeDoPKdjsLr1U8SNKGx1CMOzB77Peh',
        'status_code' => '200',
    ];

    /**
     * The "getSignRequest" response data.
     *
     * @var array
     */
    public const GET_SIGN_REQUEST = [
        'data' => [
            'document' => [
                'name' => 'End-user license agreement',
                'filename' => 'license-agreement.pdf',
                'file_size' => '224615',
                'created_at' => '2017-03-29T16:57:19+0200',
                'updated_at' => '2017-03-29T16:57:19+0200',
            ],
            'sign_request' => [
                'key' => 'eIGy1HCynD6B7QDo2gbsXeDoPKdjsLr1U8SNKGx1CMOzB77Peh',
                'sent' => 1,
                'sent_at' => '2017-03-29T16:58:16+0200',
                'all_signed' => 0,
                'all_signed_at' => '0000-00-00 00:00:00',
                'signed_hash' => '',
                'proof_hash' => '',
                'message' => 'Hi! Could you please sign our license agreement? Cheers!',
                'comment' => 'Customer wants to start asap',
                'external_key' => 'myexternalkey12345',
                'call_back_url' => 'https://www.myawesomewebsitecallbackurl.com/callback',
                'created_at' => '2017-03-29T16:57:19+0200',
                'updated_at' => '2017-03-29T16:58:16+0200',
                'terms' => [
                    [
                        'term_code' => '1w',
                        'term_expired' => 1,
                        'term_expires' => '2017-04-05T16:58:16+0200',
                        'created_at' => '2017-03-29T16:57:19+0200',
                    ],
                    [
                        'term_code' => '2d',
                        'term_expired' => 0,
                        'term_expires' => '2017-04-08T09:00:00+0200',
                        'created_at' => '2017-04-06T09:00:00+0200',
                    ],
                ],
                'signers' => [
                    [
                        'key' => 'X7FylzU1MQlIaMEo4oec03lkQtI0wF2jtrEHQoXtotGDaDH5CI',
                        'email' => 'signer@stiply.nl',
                        'name' => null,
                        'order' => null,
                        'phone' => 31612345678,
                        'auth_method' => 'sms',
                        'created_at' => '2017-03-29T16:58:05+0200',
                        'updated_at' => '2017-03-29T16:58:06+0200',
                        'sign_url' => null,
                        'signer_progresses' => [
                            [
                                'action' => 'mail_signrequest_sent',
                                'ip' => null,
                                'location' => null,
                                'system' => null,
                                'value' => 'signer@stiply.nl',
                                'created_at' => '2017-03-29T16:58:16+0200',
                                'status' => 'failed',
                            ],
                            [
                                'action' => 'mail_reminder_sent',
                                'ip' => null,
                                'location' => null,
                                'system' => null,
                                'value' => 'signer@stiply.nl',
                                'created_at' => '2017-03-29T16:58:27+0200',
                                'status' => null,
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ];

    /**
     * The "deleteSignRequest" response data.
     *
     * @var array
     */
    public const DELETE_SIGN_REQUEST = [
        'message' => 'Ondertekenverzoek en document succesvol verwijderd.',
        'status_code' => '200',
    ];

    /**
     * The "getSigner" response data.
     *
     * @var array
     */
    public const GET_SIGNER = [
        'data' => [
            'key' => 'xuSfW4HkRZHW2dRZipk8jIefqYVsMdWXK3QmWALbMJ216NPdRZ',
            'email' => 'signer@stiply.nl',
            'name' => null,
            'order' => 0,
            'language' => 'nl',
            'phone' => 31612345678,
            'auth_method' => 'sms',
            'role' => '',
            'sign_url' => null,
            'emandate' => [],
            'created_at' => '2017-03-29T16:58:05+0200',
            'updated_at' => '2017-03-29T16:58:06+0200',
            'signer_progresses' => [
                [
                    'action' => 'mail_signrequest_sent',
                    'ip' => null,
                    'location' => null,
                    'system' => null,
                    'value' => 'signer@stiply.nl',
                    'created_at' => '2017-03-29T16:58:16+0200',
                ],
                [
                    'action' => 'mail_reminder_sent',
                    'ip' => null,
                    'location' => null,
                    'system' => null,
                    'value' => 'signer@stiply.nl',
                    'created_at' => '2017-03-29T16:58:27+0200',
                ],
            ],
            'fields' => [
                [
                    'type' => 'signature',
                    'x' => 91.34,
                    'y' => 865.35,
                    'width' => 250,
                    'height' => 125,
                    'page' => 1,
                    'created_at' => '2017-03-29T16:58:06+0200',
                    'updated_at' => '2017-03-29T16:58:06+0200',
                ],
                [
                    'type' => 'signature',
                    'x' => 144.64,
                    'y' => 755.36,
                    'width' => 230,
                    'height' => 20,
                    'page' => 1,
                    'created_at' => '2017-03-29T16:58:06+0200',
                    'updated_at' => '2017-03-29T16:58:06+0200',
                ],
            ],
        ],
    ];

    /**
     * The "updateSigner" response data.
     *
     * @var array
     */
    public const UPDATE_SIGNER = [
        'data' => [
            'key' => 'xuSfW4HkRZHW2dRZipk8jIefqYVsMdWXK3QmWALbMJ216NPdRZ',
            'email' => 'newemail@stiply.nl',
            'name' => null,
            'order' => 0,
            'language' => 'nl',
            'phone' => 31612345678,
            'auth_method' => 'sms',
            'role' => '',
            'sign_url' => null,
            'emandate' => [],
            'created_at' => '2017-03-29T16:58:05+0200',
            'updated_at' => '2017-03-29T16:58:06+0200',
            'signer_progresses' => [
                [
                    'action' => 'mail_signrequest_sent',
                    'ip' => null,
                    'location' => null,
                    'system' => null,
                    'value' => 'signer@stiply.nl',
                    'created_at' => '2017-03-29T16:58:16+0200',
                ],
                [
                    'action' => 'mail_reminder_sent',
                    'ip' => null,
                    'location' => null,
                    'system' => null,
                    'value' => 'signer@stiply.nl',
                    'created_at' => '2017-03-29T16:58:27+0200',
                ],
            ],
            'fields' => [
                [
                    'type' => 'signature',
                    'x' => 91.34,
                    'y' => 865.35,
                    'width' => 250,
                    'height' => 125,
                    'page' => 1,
                    'created_at' => '2017-03-29T16:58:06+0200',
                    'updated_at' => '2017-03-29T16:58:06+0200',
                ],
                [
                    'type' => 'signature',
                    'x' => 144.64,
                    'y' => 755.36,
                    'width' => 230,
                    'height' => 20,
                    'page' => 1,
                    'created_at' => '2017-03-29T16:58:06+0200',
                    'updated_at' => '2017-03-29T16:58:06+0200',
                ],
            ],
        ],
    ];

    /**
     * The "deleteSigner" response data.
     *
     * @var array
     */
    public const DELETE_SIGNER = [
        'message' => 'Ondertekenaar succesvol verwijderd.',
        'status_code' => '200',
    ];
}
