<?php

namespace Tests\Stubs;

use Stiply\Contracts\Auth\TokenRepository as TokenRepositoryContract;

final class TokenRepository implements TokenRepositoryContract
{
    /**
     * Return a Stiply compatible auth token.
     *
     * @return string
     */
    public function token() : string
    {
        return 'my-token';
    }
}
