<?php

namespace Tests\Stubs;

use Illuminate\Support\Str;
use Stiply\Support\DataObject as Base;

final class DataObject extends Base
{
    /**
     * Custom accessor.
     *
     * @param  string  $value
     * @return string
     */
    public function getSlugMessageAttribute(string $value) : string
    {
        return Str::slug($value, '-');
    }

    /**
     * Custom mutator.
     *
     * @param  string  $value
     * @return void
     */
    public function setTitleMessageAttribute(string $value) : void
    {
        $this->attributes['title_message'] = Str::title($value);
    }
}
