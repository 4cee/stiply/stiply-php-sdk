<?php

namespace Stiply\Exceptions;

use Symfony\Component\HttpKernel\Exception\HttpException;

class StiplyException extends HttpException
{
}
