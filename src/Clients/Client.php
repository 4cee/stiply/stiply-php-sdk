<?php

namespace Stiply\Clients;

use Closure;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Stiply\Config\Config;
use Stiply\Contracts\Auth\TokenRepository;
use Stiply\Contracts\Clients\Client as ClientContract;
use Stiply\Exceptions\AuthException;
use Stiply\Exceptions\StiplyException;

class Client implements ClientContract
{
    /**
     * The Guzzle client implementation.
     *
     * @var \GuzzleHttp\Client
     */
    private $client;

    /**
     * The token repository implementation.
     *
     * @var \Stiply\Contracts\Auth\TokenRepository
     */
    private $tokens;

    /**
     * Create a new client instance.
     *
     * @param  \GuzzleHttp\Client  $client
     * @param  \Stiply\Contracts\Auth\TokenRepository  $tokens
     * @return void
     */
    public function __construct(GuzzleClient $client, TokenRepository $tokens)
    {
        $this->client = $client;
        $this->tokens = $tokens;
    }

    /**
     * Factory for creating new client instances.
     *
     * @param  \Stiply\Contracts\Auth\TokenRepository  $tokens
     * @return static
     */
    public static function create(TokenRepository $tokens) : ClientContract
    {
        $config = Config::getInstance();

        $options = array_merge(['base_uri' => static::buildUrl()], $config['guzzle'] ?? []);

        $client = new GuzzleClient($options);

        return new static($client, $tokens);
    }

    /**
     * Create a new client instance that uses a mock handler (suitable for testing).
     *
     * @param  \Stiply\Contracts\Auth\TokenRepository  $tokens
     * @param  \GuzzleHttp\Handler\MockHandler  $handler
     * @return static
     */
    public static function mock(TokenRepository $tokens, MockHandler $handler) : ClientContract
    {
        $config = Config::getInstance();

        $options = array_merge([
            'base_uri' => static::buildUrl(), 'handler' => HandlerStack::create($handler),
        ], $config['guzzle'] ?? []);

        $client = new GuzzleClient($options);

        return new static($client, $tokens);
    }

    /**
     * Perform a GET request.
     *
     * @param  string  $path
     * @param  array  $options
     * @return \GuzzleHttp\Psr7\Response
     */
    public function get(string $path, array $options = []) : Response
    {
        $options = $this->mergeOptions($options);

        return $this->try(function () use ($path, $options) {
            return $this->client->get($path, $options);
        });
    }

    /**
     * Perform a POST request.
     *
     * @param  string  $path
     * @param  array  $options
     * @return \GuzzleHttp\Psr7\Response
     */
    public function post(string $path, array $options = []) : Response
    {
        $options = $this->mergeOptions($options);

        return $this->try(function () use ($path, $options) {
            return $this->client->post($path, $options);
        });
    }

    /**
     * Perform a PUT request.
     *
     * @param  string  $path
     * @param  array  $options
     * @return \GuzzleHttp\Psr7\Response
     */
    public function put(string $path, array $options = []) : Response
    {
        $options = $this->mergeOptions($options);

        return $this->try(function () use ($path, $options) {
            return $this->client->put($path, $options);
        });
    }

    /**
     * Perform a DELETE request.
     *
     * @param  string  $path
     * @param  array  $options
     * @return \GuzzleHttp\Psr7\Response
     */
    public function delete(string $path, array $options = []) : Response
    {
        $options = $this->mergeOptions($options);

        return $this->try(function () use ($path, $options) {
            return $this->client->delete($path, $options);
        });
    }

    /**
     * Merge any custom request options with the default request options.
     *
     * @param  array  $options
     * @return array
     */
    private function mergeOptions(array $options) : array
    {
        $auth = ['Authorization' => $this->auth()];

        $options['headers'] = \array_key_exists('headers', $options)
            ? array_merge($auth, $options['headers'])
            : $auth;

        return $options;
    }

    /**
     * Attempt to execute the given closure.
     *
     * @param  \Closure  $fn
     *
     * @throws \Stiply\Exceptions\StiplyException
     * @return \GuzzleHttp\Psr7\Response
     */
    private function try(Closure $fn) : Response
    {
        try {
            return $fn();
        } catch (RequestException $e) {
            throw new StiplyException(...static::parseException($e));
        }
    }

    /**
     * Return the OAuth2 authorisation header value using the provided token
     * repository instance.
     *
     * @return string
     */
    private function auth() : string
    {
        $config = Config::getInstance();

        $version = $config['version'];
        $auth = $config['auth'];

        $this->checkAuthMethod($auth);
        $this->checkApiVersionAgainstAuthMethod($version, $auth);

        if ($auth === 'oauth' || $auth === 'jwt') {
            return 'Bearer '.$this->tokens->token();
        }

        return 'Basic '.$this->tokens->token();
    }

    /**
     * Check if the configured API auth method is a valid choice.
     *
     * @param  string  $method
     *
     * @throws \Stiply\Exceptions\AuthException
     * @return void
     */
    private function checkAuthMethod(string $method) : void
    {
        if (!\in_array($method, ['basic', 'jwt', 'oauth'], true)) {
            throw new AuthException('Auth method ['.$method.'] is invalid.');
        }
    }

    /**
     * Check if the configured API version is compatible with the configured
     * API auth method.
     *
     * @param  string  $version
     * @param  string  $auth
     *
     * @throws \Stiply\Exceptions\AuthException
     * @return void
     */
    private function checkApiVersionAgainstAuthMethod(string $version, string $auth) : void
    {
        if (($version === 'v1' && !($auth === 'basic' || $auth === 'jwt'))
            || ($version !== 'v1' && $auth !== 'oauth')) {
            throw new AuthException('Auth method ['.$auth.'] for API ['.$version.'] is invalid.');
        }
    }

    /**
     * Build the full base URL.
     *
     * @return string
     */
    private static function buildUrl() : string
    {
        $config = Config::getInstance();

        return rtrim($config['base_url'], '/').'/'.$config['version'].'/';
    }

    /**
     * Parse the Guzzle request exception.
     *
     * @param  \GuzzleHttp\Exception\RequestException  $exception
     * @return array
     */
    private static function parseException(RequestException $exception) : array
    {
        if ($exception->hasResponse()) {
            $statusCode = $exception->getResponse()->getStatusCode();
            $message = $exception->getResponse()->getBody();
        } else {
            $statusCode = 500;
            $message = $exception->getMessage();
        }

        return [$statusCode, $message];
    }
}
