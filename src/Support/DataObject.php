<?php

namespace Stiply\Support;

use ArrayAccess;
use GuzzleHttp\Psr7\Response;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use JsonSerializable;

abstract class DataObject implements ArrayAccess, JsonSerializable
{
    /**
     * The data object attributes.
     *
     * @var array
     */
    protected $attributes;

    /**
     * Create a new data object instance.
     *
     * @param  array  $attributes
     * @return void
     */
    public function __construct(array $attributes = [])
    {
        $this->attributes = $attributes;
    }

    /**
     * Get the data object's attribute.
     *
     * @param  string  $key
     * @return mixed
     */
    public function __get(string $key)
    {
        return $this->getAttribute($key);
    }

    /**
     * Set the data object's attribute.
     *
     * @param  string  $key
     * @param  mixed  $value
     * @return void
     */
    public function __set(string $key, $value) : void
    {
        $this->setAttribute($key, $value);
    }

    /**
     * Check if the data object's attribute is set.
     *
     * @param  string  $key
     * @return bool
     */
    public function __isset(string $key) : bool
    {
        return $this->hasAttribute($key);
    }

    /**
     * Convert the object to its string representation.
     *
     * @return string
     */
    public function __toString() : string
    {
        return json_encode($this->jsonSerialize());
    }

    /**
     * Unset an attribute on the data object.
     *
     * @param  string  $key
     * @return void
     */
    public function __unset(string $key) : void
    {
        $this->forgetAttribute($key);
    }

    /**
     * Get the value for a given offset.
     *
     * @param  mixed  $offset
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return $this->getAttribute($offset);
    }

    /**
     * Set the value for a given offset.
     *
     * @param  mixed  $offset
     * @param  mixed  $value
     * @return void
     */
    public function offsetSet($offset, $value) : void
    {
        $this->setAttribute($offset, $value);
    }

    /**
     * Determine if the given attribute exists.
     *
     * @param  mixed  $offset
     * @return bool
     */
    public function offsetExists($offset) : bool
    {
        return $this->hasAttribute($offset);
    }

    /**
     * Unset the value for a given offset.
     *
     * @param  mixed  $offset
     * @return void
     */
    public function offsetUnset($offset) : void
    {
        $this->forgetAttribute($offset);
    }

    /**
     * Get an attribute from the $attributes array.
     *
     * @param  string  $key
     * @return mixed
     */
    public function getAttribute(string $key)
    {
        $value = Arr::get($this->attributes, $key);

        // Check for the presence of an accessor
        if (method_exists($this, 'get'.Str::studly($key).'Attribute')) {
            $method = 'get'.Str::studly($key).'Attribute';

            return $this->{$method}($value);
        }

        return $value;
    }

    /**
     * Set a given attribute on the data object.
     *
     * @param  string  $key
     * @param  mixed  $value
     * @return void
     */
    public function setAttribute(string $key, $value) : void
    {
        // Check for the presence of a mutator
        if (method_exists($this, 'set'.Str::studly($key).'Attribute')) {
            $method = 'set'.Str::studly($key).'Attribute';

            $this->{$method}($value);
        } else {
            Arr::set($this->attributes, $key, $value);
        }
    }

    /**
     * Determine if the given attribute exists on the data object.
     *
     * @param  string  $key
     * @return bool
     */
    public function hasAttribute(string $key) : bool
    {
        return Arr::has($this->attributes, $key);
    }

    /**
     * Remove the given attribute from the data object.
     *
     * @param  string  $key
     * @return void
     */
    public function forgetAttribute(string $key) : void
    {
        Arr::forget($this->attributes, $key);
    }

    /**
     * Get the instance as an array.
     *
     * @return array
     */
    public function toArray() : array
    {
        return $this->attributes;
    }

    /**
     * Convert the object into something JSON serializable.
     *
     * @return array
     */
    public function jsonSerialize() : array
    {
        return $this->toArray();
    }

    /**
     * Create a new data object from the given response.
     *
     * @param  \GuzzleHttp\Psr7\Response  $response
     * @return static
     */
    public static function fromResponse(Response $response)
    {
        $body = json_decode($response->getBody(), true);

        return $body === null ? new static() : new static($body);
    }
}
