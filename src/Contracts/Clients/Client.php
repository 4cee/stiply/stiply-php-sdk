<?php

namespace Stiply\Contracts\Clients;

interface Client
{
    /**
     * Perform a GET request.
     *
     * @param  string  $path
     * @param  array  $options
     * @return array|string|null
     */
    public function get(string $path, array $options);

    /**
     * Perform a POST request.
     *
     * @param  string  $path
     * @param  array  $options
     * @return array|string|null
     */
    public function post(string $path, array $options);

    /**
     * Perform a PUT request.
     *
     * @param  string  $path
     * @param  array  $options
     * @return array|string|null
     */
    public function put(string $path, array $options);

    /**
     * Perform a DELETE request.
     *
     * @param  string  $path
     * @param  array  $options
     * @return array|string|null
     */
    public function delete(string $path, array $options);
}
