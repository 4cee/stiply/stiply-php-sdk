<?php

namespace Stiply\Contracts;

use Stiply\CreateSigner;
use Stiply\CreateSignRequest;
use Stiply\DeleteSigner;
use Stiply\DeleteSignRequest;
use Stiply\ExtendTerm;
use Stiply\GetProofDocument;
use Stiply\GetSignedDocument;
use Stiply\GetSigner;
use Stiply\GetSignRequest;
use Stiply\GetSignRequestKey;
use Stiply\Ping;
use Stiply\SendReminder;
use Stiply\SendSignRequest;
use Stiply\UpdateSigner;

interface Api
{
    /**
     * Ping the API.
     *
     * @param  array  $parameters
     * @return \Stiply\Ping
     */
    public function ping(array $parameters) : Ping;

    /**
     * Create a new sign request.
     *
     * @param  array  $parameters
     * @return \Stiply\CreateSignRequest
     */
    public function createSignRequest(array $parameters) : CreateSignRequest;

    /**
     * Add a signer to a sign request.
     *
     * @param  string  $signRequest
     * @param  array  $parameters
     * @return \Stiply\CreateSigner
     */
    public function createSigner(string $signRequest, array $parameters) : CreateSigner;

    /**
     * Send a sign request to the first signer.
     *
     * @param  string  $signRequest
     * @param  array  $parameters
     * @return \Stiply\SendSignRequest
     */
    public function sendSignRequest(string $signRequest) : SendSignRequest;

    /**
     * Get the signed document.
     *
     * @param  string  $signRequest
     * @return \Stiply\GetSignedDocument
     */
    public function getSignedDocument(string $signRequest) : GetSignedDocument;

    /**
     * Get the proof document.
     *
     * @param  string  $signRequest
     * @return \Stiply\GetProofDocument
     */
    public function getProofDocument(string $signRequest) : GetProofDocument;

    /**
     * Send a reminder mail to the current signer of a sign request.
     *
     * @param  string  $signRequest
     * @return \Stiply\SendReminder
     */
    public function sendReminder(string $signRequest) : SendReminder;

    /**
     * Extend the term of a sign request.
     *
     * @param  string  $signRequest
     * @param  array  $parameters
     * @return \Stiply\ExtendTerm
     */
    public function extendTerm(string $signRequest, array $parameters) : ExtendTerm;

    /**
     * Get a sign request key using an external key.
     *
     * @param  string  $key
     * @return \Stiply\GetSignRequestKey
     */
    public function getSignRequestKey(string $key) : GetSignRequestKey;

    /**
     * Get a sign request.
     *
     * @param  string  $signRequest
     * @return \Stiply\GetSignRequest
     */
    public function getSignRequest(string $signRequest) : GetSignRequest;

    /**
     * Delete a sign request.
     *
     * @param  string  $signRequest
     * @return \Stiply\DeleteSignRequest
     */
    public function deleteSignRequest(string $signRequest) : DeleteSignRequest;

    /**
     * Get a signer of a sign request.
     *
     * @param  string  $signRequest
     * @param  string  $signer
     * @return \Stiply\GetSigner
     */
    public function getSigner(string $signRequest, string $signer) : GetSigner;

    /**
     * Update a signer of a sign request.
     *
     * @param  string  $signRequest
     * @param  string  $signer
     * @param  array  $parameters
     * @return \Stiply\UpdateSigner
     */
    public function updateSigner(string $signRequest, string $signer, array $parameters) : UpdateSigner;

    /**
     * Delete a signer of a sign request.
     *
     * @param  string  $signRequest
     * @param  string  $signer
     * @return \Stiply\DeleteSigner
     */
    public function deleteSigner(string $signRequest, string $signer) : DeleteSigner;
}
