<?php

namespace Stiply\Contracts\Auth;

interface TokenRepository
{
    /**
     * Return a Stiply compatible auth token.
     *
     * @return string
     */
    public function token() : string;
}
