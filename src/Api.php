<?php

namespace Stiply;

use Exception;
use GuzzleHttp\Handler\MockHandler;
use Illuminate\Support\Arr;
use Stiply\Clients\Client;
use Stiply\Contracts\Api as ApiContract;
use Stiply\Contracts\Auth\TokenRepository;

final class Api implements ApiContract
{
    /**
     * The client implementation.
     *
     * @var \Stiply\Contracts\Clients\Client
     */
    private $client;

    /**
     * Create a new API instance.
     *
     * @param  \Stiply\Contracts\Clients\Client  $client
     * @return void
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * Factory for creating new API instances.
     *
     * @param  \Stiply\Contracts\Auth\TokenRepository  $tokens
     * @return static
     */
    public static function create(TokenRepository $tokens) : ApiContract
    {
        $client = Client::create($tokens);

        return new self($client);
    }

    /**
     * Create a new API instance that uses a mock handler (suitable for testing).
     *
     * @param  \Stiply\Contracts\Auth\TokenRepository  $tokens
     * @param  \GuzzleHttp\Handler\MockHandler  $handler
     * @return static
     */
    public static function mock(TokenRepository $tokens, MockHandler $handler) : ApiContract
    {
        $client = Client::mock($tokens, $handler);

        return new self($client);
    }

    /**
     * Ping the API.
     *
     * @param  array  $parameters
     * @return \Stiply\Ping
     */
    public function ping(array $parameters) : Ping
    {
        $parameters = Arr::only($parameters, ['ping_value']);

        return Ping::fromResponse(
            $this->client->post('actions/ping', ['json' => $parameters])
        );
    }

    /**
     * Create a new sign request.
     *
     * @param  array  $parameters
     * @return \Stiply\CreateSignRequest
     */
    public function createSignRequest(array $parameters) : CreateSignRequest
    {
        $fields = [
            'call_back_url',
            'comment',
            'doc_name',
            'external_key',
            'file',
            'message',
            'reminder_code',
            'term',
        ];

        if (Arr::isAssoc($parameters)) {
            $parameters = $this->convertToMultipart($parameters);
        }

        $parameters = Arr::where($parameters, function (array $value) use ($fields) : bool {
            return \in_array($value['name'], $fields, true);
        });

        return CreateSignRequest::fromResponse(
            $this->client->post('sign_requests', ['multipart' => $parameters])
        );
    }

    /**
     * Add a signer to a sign request.
     *
     * @param  string  $signRequest
     * @param  array  $parameters
     * @return \Stiply\CreateSigner
     */
    public function createSigner(string $signRequest, array $parameters) : CreateSigner
    {
        $parameters = Arr::only($parameters, [
            'auth_method',
            'cell_phone_number',
            'emandate',
            'idin',
            'invitation_method',
            'language',
            'message',
            'pimid',
            'redirect_url',
            'role',
            'signer_email',
            'signer_checkbox_fields',
            'signer_date_fields',
            'signer_initial_fields',
            'signer_signature_fields',
            'signer_text_fields',
        ]);

        return CreateSigner::fromResponse(
            $this->client->post('sign_requests/'.$signRequest.'/signers', ['json' => $parameters])
        );
    }

    /**
     * Send a sign request to the first signer.
     *
     * @param  string  $signRequest
     * @param  array  $parameters
     * @return \Stiply\SendSignRequest
     */
    public function sendSignRequest(string $signRequest) : SendSignRequest
    {
        return SendSignRequest::fromResponse(
            $this->client->post('sign_requests/'.$signRequest.'/actions/send')
        );
    }

    /**
     * Get the signed document.
     *
     * @param  string  $signRequest
     * @return \Stiply\GetSignedDocument
     */
    public function getSignedDocument(string $signRequest) : GetSignedDocument
    {
        return GetSignedDocument::fromResponse(
            $this->client->get('sign_requests/'.$signRequest.'/documents/actions/get_signed_document')
        );
    }

    /**
     * Get the proof document.
     *
     * @param  string  $signRequest
     * @return \Stiply\GetProofDocument
     */
    public function getProofDocument(string $signRequest) : GetProofDocument
    {
        return GetProofDocument::fromResponse(
            $this->client->get('sign_requests/'.$signRequest.'/documents/actions/get_proof_document')
        );
    }

    /**
     * Send a reminder mail to the current signer of a sign request.
     *
     * @param  string  $signRequest
     * @return \Stiply\SendReminder
     */
    public function sendReminder(string $signRequest) : SendReminder
    {
        return SendReminder::fromResponse(
            $this->client->post('sign_requests/'.$signRequest.'/actions/send_reminder')
        );
    }

    /**
     * Extend the term of a sign request.
     *
     * @param  string  $signRequest
     * @param  array  $parameters
     * @return \Stiply\ExtendTerm
     */
    public function extendTerm(string $signRequest, array $parameters) : ExtendTerm
    {
        $parameters = Arr::only($parameters, ['term']);

        return ExtendTerm::fromResponse(
            $this->client->post('sign_requests/'.$signRequest.'/actions/extend_term', [
                'json' => $parameters,
            ])
        );
    }

    /**
     * Get a sign request key using an external key.
     *
     * @param  string  $key
     * @return \Stiply\GetSignRequestKey
     */
    public function getSignRequestKey(string $key) : GetSignRequestKey
    {
        return GetSignRequestKey::fromResponse(
            $this->client->get('sign_requests/'.$key.'/actions/get_sign_request_key')
        );
    }

    /**
     * Get a sign request.
     *
     * @param  string  $signRequest
     * @return \Stiply\GetSignRequest
     */
    public function getSignRequest(string $signRequest) : GetSignRequest
    {
        return GetSignRequest::fromResponse(
            $this->client->get('sign_requests/'.$signRequest)
        );
    }

    /**
     * Delete a sign request.
     *
     * @param  string  $signRequest
     * @return \Stiply\DeleteSignRequest
     */
    public function deleteSignRequest(string $signRequest) : DeleteSignRequest
    {
        return DeleteSignRequest::fromResponse(
            $this->client->delete('sign_requests/'.$signRequest)
        );
    }

    /**
     * Get a signer of a sign request.
     *
     * @param  string  $signRequest
     * @param  string  $signer
     * @return \Stiply\GetSigner
     */
    public function getSigner(string $signRequest, string $signer) : GetSigner
    {
        return GetSigner::fromResponse(
            $this->client->get('sign_requests/'.$signRequest.'/signers/'.$signer)
        );
    }

    /**
     * Update a signer of a sign request.
     *
     * @param  string  $signRequest
     * @param  string  $signer
     * @param  array  $parameters
     * @return \Stiply\UpdateSigner
     */
    public function updateSigner(string $signRequest, string $signer, array $parameters) : UpdateSigner
    {
        $parameters = Arr::only($parameters, ['new_email', 'new_cell_phone_number']);

        return UpdateSigner::fromResponse(
            $this->client->put('sign_requests/'.$signRequest.'/signers/'.$signer, [
                'json' => $parameters,
            ])
        );
    }

    /**
     * Delete a signer of a sign request.
     *
     * @param  string  $signRequest
     * @param  string  $signer
     * @return \Stiply\DeleteSigner
     */
    public function deleteSigner(string $signRequest, string $signer) : DeleteSigner
    {
        return DeleteSigner::fromResponse(
            $this->client->delete('sign_requests/'.$signRequest.'/signers/'.$signer)
        );
    }

    /**
     * Convert an associated array to a Guzzle compatible multipart array.
     *
     * @param  array  $parameters
     *
     * @throws \Exception
     * @return array
     */
    private function convertToMultipart(array $parameters) : array
    {
        $multipart = [];

        $filename = Arr::pull($parameters, 'filename');

        if ($filename === null) {
            throw new Exception('The filename parameter is missing.');
        }

        foreach ($parameters as $key => $value) {
            $multipart[] = $key === 'file'
                ? ['name' => $key, 'filename' => $filename, 'contents' => $value]
                : ['name' => $key, 'contents' => $value];
        }

        return $multipart;
    }
}
