<?php

namespace Stiply\Config;

use ArrayAccess;
use Illuminate\Support\Arr;
use Stiply\Contracts\Config\Config as ConfigContract;

final class Config implements ConfigContract, ArrayAccess
{
    /**
     * The default config.
     *
     * @var array
     */
    public const DEFAULT_ITEMS = [
        'base_url' => 'https://api.stiply.nl/',
        'version' => 'v1.1',
        'auth' => 'oauth',
    ];

    /**
     * The current globally available config instance (if any).
     *
     * @var static|null
     */
    private static $instance;

    /**
     * The config keys and values.
     *
     * @var array
     */
    private $items;

    /**
     * Create a new config instance.
     *
     * @return void
     */
    private function __construct()
    {
        $this->items = self::DEFAULT_ITEMS;
    }

    /**
     * Get the globally available instance of the config.
     *
     * @return static
     */
    public static function getInstance() : ConfigContract
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Set the shared instance of the config.
     *
     * @param  \Stiply\Contracts\Config|null  $container
     * @return \Stiply\Contracts\Config|static
     */
    public static function setInstance(?ConfigContract $config = null)
    {
        return self::$instance = $config;
    }

    /**
     * Determine if the given key exists.
     *
     * @param  string  $key
     * @return bool
     */
    public function has(string $key) : bool
    {
        return Arr::has($this->items, $key);
    }

    /**
     * Get the value for the given key.
     *
     * @param  string  $key
     * @return mixed
     */
    public function get(string $key)
    {
        return Arr::get($this->items, $key);
    }

    /**
     * Set the value for the given key.
     *
     * @param  string  $key
     * @param  mixed  $value
     * @return void
     */
    public function set(string $key, $value) : void
    {
        Arr::set($this->items, $key, $value);
    }

    /**
     * Determine if the value for the given key exists.
     *
     * @param  string  $key
     * @return bool
     */
    public function offsetExists($key)
    {
        return $this->has($key);
    }

    /**
     * Get the value for the given key.
     *
     * @param  string  $key
     * @return mixed
     */
    public function offsetGet($key)
    {
        return $this->get($key);
    }

    /**
     * Set the value for the given key.
     *
     * @param  string  $key
     * @param  mixed  $value
     * @return void
     */
    public function offsetSet($key, $value) : void
    {
        $this->set($key, $value);
    }

    /**
     * Unset the value for the given key.
     *
     * @param  string  $key
     * @return void
     */
    public function offsetUnset($key) : void
    {
        $this->set($key, null);
    }
}
