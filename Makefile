autoload:
	composer dump-autoload

lint:
	./vendor/bin/php-cs-fixer fix --config=.php-cs-fixer.dist.php -v --using-cache=yes --allow-risky=yes ./

test:
	./vendor/bin/phpunit --bootstrap ./vendor/autoload.php tests
