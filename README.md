# Stiply PHP SDK

An official library for using the Stiply API written in PHP.

## Table of Contents

- [Requirements](#requirements)
- [Installation](#installation)
- [Usage](#usage)
    - [Configuration](#configuration)
    - [Auth](#auth)
    - [Available Methods](#available-methods)
- [Example](#example)
- [Testing](#testing)

## Requirements

- [PHP](https://www.php.net/) >= 7.2.0

## Installation

This library is available through [Composer](https://getcomposer.org/):

```shell
composer require stiply/stiply-php-sdk
```

## Usage

The main purpose of this library is to make interaction with the _Stiply_ API easier. The task of having to set up a HTTP client and perform requests yourself is handled by the SDK. Instead, you call a dedicated method on an instance of `\Stiply\Api` to get the desired response. A quick example:

```php
<?php

$tokens = new \My\Custom\TokenRepository();

$api = \Stiply\Api::create($tokens);

// Returns an instance of \Stiply\Ping
$result = $api->ping(['ping_value' => 'pong']);

echo $result['ping_value'];  // Or "$result->ping_value"
echo $result['status_code'];
```

As you can see, on success, a data object will be returned that contains the body of the response from the _Stiply_ API as its attributes.

### Configuration

By default, an instance of `\Stiply\Api` is configured to work with version _v1.1_ of the _Stiply_ API, which requires _OAuth2_ for authentication / authorisation. However, it is possible to change this if you want to target another API version and form of authentication / authorisation by means of a singleton instance of `\Stiply\Config\Config`:

```php
<?php

// We want to target the "v1" version of the Stiply API instead
$config = \Stiply\Config\Config::getInstance();

$config->set('version', 'v1');
$config->set('auth', 'basic');

$tokens = new \My\Custom\TokenRepository();

$api = \Stiply\Api::create($tokens);

// Use "$api" to interact with the Stiply API
```

There are a few things to take into consideration here:

1. The `$config` variable points to a singleton instance of `\Stiply\Config\Config`. There should at any one point exist only a single instance of the configuration object, as it is used internally by the SDK itself, as well as possibly by the user of the SDK to customise some of the configurable options like the API version and auth method.
2. It is advisable to customise any configuration options _before_ creating a new instance of `\Stiply\Api`, as some configuration options (e.g. "guzzle", "base_url") are only acknowledged at the time of creating a new instance of `\Stiply\Api`.
3. The SDK uses [Guzzle](http://docs.guzzlephp.org/en/stable/) under the hood for making all HTTP requests. Hence, it is possible to customise some of the _Guzzle_ specific request options that are passed in on client creation. There rarely should be a need for this though and you should be careful with overriding any options that are set by the SDK itself (like "base_uri" for instance).

 > _**Important**_: If you use the SDK and run into a `\Stiply\Exceptions\StiplyException` with message "cURL error 60: SSL certificate problem: unable to get local issuer certificate..." then there most likely is something wrong with your server's SSL certificate or it is missing altogether. If you are testing the SDK locally and don't want to be bothered by this error you can update the "guzzle" config option like so: `$config->set('guzzle', ['verify' => false])`. This will disable SSL certificate verification. However, this is insecure and we don't recommend doing this in general.

### Auth

The SDK purposely does not concern itself with the authentication / authorisation part of connecting with the _Stiply_ API. Hence, the implementation of a suitable authentication / authorisation strategy is the sole responsibility of the user of the SDK. The approach taken here is to provide an instance of `\Stiply\Api` with an implementation of `\Stiply\Contracts\Auth\TokenRepository`. This implementation should return a valid auth token by means of the `\My\Custom\TokenRepository#token` method:

```php
<?php

namespace My\Custom;

use Stiply\Contracts\Auth\TokenRepository as TokenRepositoryContract;

class TokenRepository implements TokenRepositoryContract
{
    /**
     * Return a Stiply compatible auth token.
     *
     * @return string
     */
    public function token() : string
    {
        // Your custom logic for generating a suitable auth token for use with
        // the Stiply API. In case of API version "v1.1" this would mean
        // providing a valid (personal) access token using OAuth, in case of API
        // version "v1" this would mean the base64 encoded combination of
        // "username:password"

        return $token;
    }
}
```

An instance of your custom token repository then should be provided as an argument when creating a new instance of `\Stiply\Api`:

```php
<?php

$tokens = new \My\Custom\TokenRepository();

$api = \Stiply\Api::create($tokens);

// Use "$api" to interact with the Stiply API
```

and then the SDK will take care of constructing the appropriate authorisation header each time it makes a request to the _Stiply_ API.

> _**Important**_: If you wish to customise the default auth configuration then you should make sure that the specified "version" and "auth" options are compatible. For instance, version "v1" of the _Stiply_ API only works with auth "basic" (or "jwt" in case of certain partners) and version "v1.1" only works with auth "oauth". If the SDK detects a version / auth option mismatch an instance of `\Stiply\Exceptions\AuthException` will be thrown.

### Available Methods

All current API endpoints are represented by the following corresponding methods in the SDK:

#### `$result = $api->ping($parameters);`

Ping the _Stiply_ API. On success, `$result` will be an instance of `\Stiply\Ping` that contains the body of the response from the _Stiply_ API as its attributes. `$parameters` is an array that contains the following key / value pairs:

| Key | Value | Description |
|-----|-------|-------------|
| `ping_value` | `string` | If the response was successful, the same string will be present on `$result->ping_value`. |

#### `$result = $api->createSignRequest($parameters);`

Create a new sign request. On success, `$result` will be an instance of `\Stiply\CreateSignRequest` that contains the body of the response from the _Stiply_ API as its attributes. `$parameters` is an array that contains the following key / value pairs:

| Key | Value | Description |
|-----|-------|-------------|
| `file` | `resource` | The document (DOC, DOCX or PDF) that should be signed. |
| `filename` | `string` | The filename of the document, including the extension. This _**must**_ match the filename of the actual document, otherwise uploading the document will fail. |
| `term` | `string` | 2 digit code representing the sign term ("1d" = 1 day, "2w" = 2 weeks, "3m" = 3 months). |
| `doc_name` (optional) | `string` | The name of the document. Please note this is not the name of the file but merely a label for the document. |
| `message` (optional) | `string` | The message to be included in the mail to the signers. |
| `comment` (optional) | `string` | A comment for internal use. |
| `external_key` (optional, unique) | `string` | A key for your internal use so you don’t have to save the Stiply sign request key in your local database. However, your external key has to be unique. |
| `call_back_url` (optional) | `string` | An URL to be called by Stiply when the last signer has signed the document. Please note that `?key={sign_request_key}` shall be added to the call back url. |

#### `$result = $api->createSigner($signRequest, $parameters);`

Create a new signer for the given sign request.

##### Preconditions
- Sign request must exist
- Sign request has not been sent

On success, `$result` will be an instance of `\Stiply\CreateSigner` that contains the body of the response from the Stiply API as its attributes. `$signRequest` is a string which denotes the key of the sign request. `$parameters` is an array that contains the following key / value pairs:

| Key | Value | Description |
|-----|-------|-------------|
| `signer_email` | `string` | The e-mail address of signer. |
| `signer_signature_fields` | `array` | An Array of arrays containing signature field [information](https://www.stiply.com/api/#Createsigner), using either a tag or coordinates. |
| `signer_text_fields` (optional) | `array` | An Array of arrays containing text field [information](https://www.stiply.com/api/#Createsigner), using either a tag or coordinates. |
| `auth_method` (optional) | `string` | Authentication method for signer. Currently "sms", "emandate" and "idin" are available as a valid option. |
| `cell_phone_number` (optional) | `int` or `string` | Cellular phone number of the signer is required in case `auth_method` is set to "sms". Without setting `auth_method` to "sms" the phone number will not be saved. Phone numbers need to contain a country code as follows: `31655136642`. |
| `emandate` (optional) | `array` | You may add an optional [emandate](https://www.stiply.com/api/#Createsigner) array, when `auth_method` is set to "emandate". |
| `idin` (optional) | `array` | You may add an optional [idin](https://www.stiply.com/api/#Createsigner) array, when `auth_method` is set to "idin". |
| `language` (optional) | `string` | The two letter language code in which the signer receives correspondence. |
| `role` (optional) | `string` | A signer can have different roles. By default, the signer will need to sign the document explicitly. When the value "cc" is set as role, the signer shall be copy-only (cc). |
| `invitation_method` (optional) | `string` | If you would like to invite the signer yourself, you should set the `invitation_method` to "custom". In that case Stiply shall not send a mail with the sign request to the signer. Instead, when you send the sign request, the API shall return a unique sign link to you, which you can provide to the signer yourself. |
| `message` (optional) | `string` | You can send a specific message to a signer, that overrules the sign request message in the mail to the signer. |

#### `$result = $api->sendSignRequest($signRequest);`

Send the given sign request.

##### Preconditions
- Sign request must exist
- Sign request has not been sent
- Sign request has at least 1 signer with at least 1 signature field

On success, `$result` will be an instance of `\Stiply\SendSignRequest` that contains the body of the response from the Stiply API as its attributes. `$signRequest` is a string which denotes the key of the sign request.

#### `$result = $api->getSignedDocument($signRequest);`

Get the signed document associated with the given sign request.

##### Preconditions
- Sign request must exist
- Sign request has been signed by all signers

On success, `$result` will be an instance of `\Stiply\GetSignedDocument` that contains the body of the response from the Stiply API as its attributes. `$signRequest` is a string which denotes the key of the sign request.

#### `$result = $api->getProofDocument($signRequest);`

Get the proof document associated with the given sign request.

##### Preconditions
- Sign request must exist
- Sign request has been signed by all signers

On success, `$result` will be an instance of `\Stiply\GetProofDocument` that contains the body of the response from the Stiply API as its attributes. `$signRequest` is a string which denotes the key of the sign request.

#### `$result = $api->sendReminder($signRequest);`

Send a reminder mail to the current signer of the given sign request.

##### Preconditions
- Sign request must exist
- Sign request has been sent

On success, `$result` will be an instance of `\Stiply\SendReminder` that contains the body of the response from the Stiply API as its attributes. `$signRequest` is a string which denotes the key of the sign request.

#### `$result = $api->extendTerm($signRequest, $parameters);`

Extend the term of the given sign request.

##### Preconditions
- Sign request must exist
- Sign request has been sent
- The current term of the sign request has been expired

On success, `$result` will be an instance of `\Stiply\ExtendTerm` that contains the body of the response from the Stiply API as its attributes. `$signRequest` is a string which denotes the key of the sign request. `$parameters` is an array that contains the following key / value pairs:

| Key | Value | Description |
|-----|-------|-------------|
| `term` | `string` | 2 digit code representing the sign term ("1d" = 1 day, "2w" = 2 weeks, "3m" = 3 months). |

#### `$result = $api->getSignRequestKey($key);`

Get the sign request key using the given external key (your local key).

##### Preconditions
- Sign request must exist

On success, `$result` will be an instance of `\Stiply\GetSignRequestKey` that contains the body of the response from the Stiply API as its attributes. `$key` is a string which denotes the external key (your local key) of the sign request.

#### `$result = $api->getSignRequest($signRequest);`

Get the given sign request.

##### Preconditions
- Sign request must exist

On success, `$result` will be an instance of `\Stiply\GetSignRequest` that contains the body of the response from the Stiply API as its attributes. `$signRequest` is a string which denotes the key of the sign request.

#### `$result = $api->deleteSignRequest($signRequest);`

Delete the given sign request.

##### Preconditions
- Sign request must exist

On success, `$result` will be an instance of `\Stiply\DeleteSignRequest` that contains the body of the response from the Stiply API as its attributes. `$signRequest` is a string which denotes the key of the sign request.

#### `$result = $api->getSigner($signRequest, $signer);`

Get the given signer of the given sign request.

##### Preconditions
- Sign request must exist
- Signer must exist

On success, `$result` will be an instance of `\Stiply\GetSigner` that contains the body of the response from the Stiply API as its attributes. `$signRequest` is a string which denotes the key of the sign request. `$signer` is a string which denotes the key of the signer.

#### `$result = $api->updateSigner($signRequest, $signer, $parameters);`

Update the e-mail address and / or telephone number of a signer who has not yet signed. It is required to post both values, email and phone, when updating. If you only want to update one of these, just submit the original value of the other.

Please note that in case a signer has already received the sign request mail and you update the e-mail address of the signer afterwards, the link in the signer’s mail will not work anymore. The signer shall not receive a new sign request mail on the new e-mail address. Please use `\Stiply\Api#sendReminder` for that purpose after the e-mail address has been updated. If you only update the phone number the original sign link still works so no reminder is required.

##### Preconditions
- Sign request must exist
- Signer must exist
- Signer has not signed

On success, `$result` will be an instance of `\Stiply\UpdateSigner` that contains the body of the response from the Stiply API as its attributes. `$signRequest` is a string which denotes the key of the sign request. `$signer` is a string which denotes the key of the signer. `$parameters` is an array that contains the following key / value pairs:

| Key | Value | Description |
|-----|-------|-------------|
| `new_email` | `string` | New e-mail address of the signer to which the reminder mail will be send. This value will replace the e-mail address of the signer permanently. |
| `new_cell_phone_number` | `int` or `string` | New phone number of the signer to which the authentication SMS will be send. This value will replace the phone number of the signer permanently. |

#### `$result = $api->deleteSigner($signRequest, $signer);`

Delete the given signer.

##### Preconditions
- Sign request must exist
- Sign request has not been sent
- Signer must exist

On success, `$result` will be an instance of `\Stiply\DeleteSigner` that contains the body of the response from the Stiply API as its attributes. `$signRequest` is a string which denotes the key of the sign request. `$signer` is a string which denotes the key of the signer.

## Example

The following example demonstrates how to create a new sign request, add a signer, and finally send the sign request using the SDK.

The first step will be to create a new instance of the `\Stiply\Api` class, which we can then use to communicate with the _Stiply_ API:

```php
<?php

$tokens = new \My\Custom\TokenRepository();

$api = \Stiply\Api::create($tokens);
```

Next, we will create a new sign request:

```php
<?php

// Previously discussed code is here

$result = $api->createSignRequest([
    'file' => \file_get_contents('/some/path/to/a/valid/document/foo.pdf'),
    'filename' => 'foo.pdf', // Without this uploading the doc will fail!
    'term' => '1w',
    'doc_name' => 'SDK Test',
    'comment' => 'Testing the SDK',
]);

$key = $result['data']['signRequest']['key'];
```

We can now add a signer to the newly created sign request:

```php
<?php

// Previously discussed code is here

$result = $api->createSigner($key, [
    'signer_email' => 'signer@stiply.nl',
    'signer_signature_fields' => [['name' => 'signature_0']],
    'auth_method' => 'sms',
    'cell_phone_number' => 31612345678,
]);
```

> _**Important**_ It is assumed that our previously uploaded document `foo.pdf` contains one or more tags that are formatted as `{{signature_0}}`.

Finally, we can send the sign request to the first (and in this case only) signer:

```php
<?php

// Previously discussed code is here

$result = $api->sendSignRequest($key);
```

This signer should now receive a mail with a link to our viewer application where one can sign the uploaded document digitally.

In summary, the complete code for creating a new sign request, adding a single signer and sending the sign request will look something like:

```php
<?php

$tokens = new \My\Custom\TokenRepository();

$api = \Stiply\Api::create($tokens);

// Create a new sign request and upload the document to be signed
$result = $api->createSignRequest([
    'file' => \file_get_contents('/some/path/to/a/valid/document/foo.pdf'),
    'filename' => 'foo.pdf', // Without this uploading the doc will fail!
    'term' => '1w',
    'doc_name' => 'SDK Test',
    'comment' => 'Testing the SDK',
]);

// We will need the sign request key for the next steps
$key = $result['data']['signRequest']['key'];

// Create a new signer for the sign request referred to by "$key"
$result = $api->createSigner($key, [
    'signer_email' => 'signer@stiply.nl',
    'signer_signature_fields' => [['name' => 'signature_0']],
    'auth_method' => 'sms',
    'cell_phone_number' => 31612345678,
]);

// Send the sign request referred to by "$key"
$result = $api->sendSignRequest($key);
```

## Testing

It is possible to mock an instance of `\Stiply\Api`. This may be convenient for your own testing purposes:

```php
<?php

$tokens = new \My\Custom\TokenRepository();

$handler = new \GuzzleHttp\Handler\MockHandler([
    new Response(200, [], \json_encode([...])),
]);

$api = \Stiply\Api::mock($tokens, $handler);

// Perform your own testing
```

Also see the relevant _Guzzle_ [documentation](http://docs.guzzlephp.org/en/stable/testing.html).
